#define __SysTickTimer_c__
#define __ModuloSysTick_c__
/*==================[inclusions]=============================================*/
//#include <LPC17xx.h>

#include <chip.h>



//#include <FSM_Pulsador.h>/* Agregamos el Archivo de cavecera que contiene los prototipos de las Funciones de la FSM */
#include <SysTickTimer.h>
/** \addtogroup SysTickTimer_h SysTickTimer module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    SysTickTimer.c
 * @brief	Archivo que contine las Declaracion de APIs del modulo SysTickTimer.
 * Configuracion, lectura del Acumulador, chequeo de tiempo (con actuaizacio
 * automatica del registro correspondiete).
 * @version v01.01
 * @date   12/3/2016
 * @note none
 * @par  jel
 *************************************************************************************/


/*============[internal functions declaration]================================*/

/*============[internal data declaration]================================*/

/** @} doxygen end group definition */

/*============[external data declaration]================================*/
//extern FSM_Pulsador hdlFsmPulsador0;
/* Definimos una varible correspondiente al
 	 	 	 	 	 	 	 	 al handler de la Maquina de estado
 	 	 	 	 	 	 	 	 con esta podremos identificar la maquina de
 	 	 	 	 	 	 	 	 estados, cuando se haga uso de las APIS generadas */


/*==================[internal function definition]===============================*/
/********************************************************************************
* @ Descripcion:	Funcion del ISR para el SysTickTimer
*
* @ Parametro In:	No Aplica
*
* @ return:	No Aplica
*
* @ HeaderFile: "SysTickTimer.h"
*******************************************************************************/
void SysTick_Handler(void)
{
	/*>> Clear Counter flag */
	SysTick->CTRL &= ~ST_CTRL_COUNTFLAG; // Bajamos la Bandera de Interupcion
	AcuMs++;

	/* Agregamos el llamado a la api de Refresco de Registro de Eventos de la
	 * maquina de estado*/
	/*fSM_PulsadorIface_raise_eSysTick(&hdlFsmPulsador0);
	fSM_Pulsador_runCycle(&hdlFsmPulsador0);*/
}





/*==================[external functions definition]==========================*/
/********************************************************************************
* @ Descripcion:	Funcion para la configuracion del SysTickTimer, para generar
* 					una interupcion cada 1 [ms]
*
* @ Parametro In:	No Aplica
*
* @ return:	No Aplica
*
* @ HeaderFile: "SysTickTimer.h"
*******************************************************************************/
void ConfigSysTick(void)
{
	/* Inicilizacion SysTick Timer
	 * Clock Interno
	 * Generacion de Interupcion cada 1 mS
	 * */
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 1000);
}


/********************************************************************************
* @ Descripcion:	Funcion para obtener el valor Actual del Acumulador de mili
* 					segundos.
*
* @ Parametro In:	No Aplica
*
* @ return unsigned int:	La cuenta de mili segundos Actual
*
* @ HeaderFile: "SysTickTimer.h"
*******************************************************************************/
unsigned int GetAcuMsSysTick(void)
{
	return AcuMs;
}



/********************************************************************************
* @ Descripcion:	Funcion para Consultar el tiempo transcurido entre
* 					llamados, y actualizar la referencia temporal
*
* @ Parametro In:
* 					#_ unsigned int Time: Valor en mili segundos, contra el cual
* 					se consulta
*
* 					#_ unsigned int *ptrRef: Puntero a la variable que toma como
* 					referancia, y sobre la cual actualiza el valor, en caso de
* 					cumplirse (AcuMs-(Ref))>Time
*
* @ return:
* 				#_ Bool:
* 				- TRUE Se cumple que [(AcuMs-Ref)>Time] y Actualiza la
* 				variable de referencia
* 				-FALSE No se cumple la condicion y no actualiza la variable
* 				de referencia.
*
* @ HeaderFile: "SysTickTimer.h"
*******************************************************************************/
Bool ConsultTime(unsigned int Time, unsigned int *ptrRef)
{
	if((AcuMs-(*ptrRef))>Time)
	{
		(*ptrRef) = AcuMs;
		return TRUE;
	}
	return FALSE;
}

/********************************************************************************
* @ Descripcion:	Iniicaliza el Acumulador de Mili segundo perteneciente a la
* Funcion Periodica
*
* @ Parametro In	*ptrAcuTime
*
* @ return:	None
*
* @ HeaderFile: SysTickTimer.c
*******************************************************************************/
void IniAcuTime(unsigned int *ptrAcuMs)
{
	(*ptrAcuMs) = AcuMs;
}

