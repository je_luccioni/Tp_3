#define __DriverWH1602C_2C4D_c__
/*==================[inclusions]=============================================*/
#include <chip_lpc175x_6x.h>
#include <DriverWH1602C_2C4D.h>


/** \addtogroup DriverWH1602C_2C4D_c  DriverWH1602C_2C4D module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    DriverWH1602C_2C4D.c
 * @brief	Archivo que contine las Definiciones de las APIs del modulo DriverWH1602C_2C4D
 * <descripcionBreve>
 * @version v0x.0y
 * @date   19/5/2016
 * @note none
 * @par  jel
 *************************************************************************************/


/** \addtogroup  DriverWH1602C_2C4D_c  DriverWH1602C_2C4D module
 ** @{ */ /*en caso de necesitar documentar las definiciones internas*/

/*==============================[]==============================*/

/*============================[internal macros]============================*/
/**
 * @brief configuration mode write Instruction
 * */
#define __ConfigWriteInstLCD(); LPC_GPIO[PortBusCtrl].CLR |= MaskPinRS;

/**
 * @brief Configuration mode write Data
 * */
#define __ConfigWriteDatoLCD();	LPC_GPIO[PortBusCtrl].SET |= MaskPinRS;

/**
 * @brief Configuration Bus of Data
 * */
#define __ConfigBusDatoLCD();	{\
	LPC_GPIO[PortBusDato].DIR |= MaskBusDato; \
	LPC_GPIO[PortBusDato].MASK &= (~MaskBusDato);\
	}

/**
 * @brief Configuration Bus of Data
 * */
#define __ConfigBusControlLCD();	{\
			LPC_GPIO[PortBusCtrl].DIR |= MaskBusCtrl; \
			LPC_GPIO[PortBusCtrl].MASK &= (~MaskBusCtrl); \
}



/*=====================[internal, typedef definition]======================*/



/*=======================[internal data declaration]=======================*/
uint8_t ptrDDRAM, ptrWindows, ptrCursor, statusDisplay;
/* prtDisplay -> ptrWindows, ventana de visualizacion, sobre memoria DDRAM LCD
 *
 * Fx -> GetPunteroWH1602C(enumTypePtr IdPtr);
 *  */

/*====================[internal, functions definition]=====================*/

/***********************************************************************
 * @ Descripcion: Generacion de retardo/delay en rango de
 * 	 			  los Nano-Segundos
 * 					+ Uso de Loop sin Cuerpo
 * 					+ Solo para inicilizacion del Modulo LCD
 * @ Parametro In:
 * 					#_ DelayMs -> Cantidad de Mili segundos
 *
 * @ Parametro Retorno: 		None
 **********************************************************************/
static void CicloEnable(void)
{
	uint8_t nDelayNs =76;
	LPC_GPIO[PortBusCtrl].SET |= MaskPinE;
	while(--nDelayNs);
	LPC_GPIO[PortBusCtrl].CLR |= MaskPinE;
}

/***********************************************************************
 * @ Descripcion: Genercion de retardo/delay en rango de
 * 	 			  los Micros-Segundos
 * 					+ Uso de Loop sin Cuerpo
 * 					+ Solo para retardo entre envio suscesivos de
 * 					de caracteres al modulo LCD
 * @ Parametro In:
 * 					#_ DelayUs -> Cantidad de Micro segundos
 *
 * @ Parametro Retorno: 		None
 **********************************************************************/
void DelayUs(uint32_t nDelayUs)
{
	nDelayUs = nDelayUs*_Fosc;
	while(--nDelayUs);
}


/*********************************************************************//**
 * @ Descripcion: Cargamos el Nibble del Dato sobre el bus de datos
 * 	 -----------  del modulo LCD
 * @ Formato Uso:  DatoLCD = [dato]; \ LoadDatoLCD();
 * 	 ------- ---
 *
 * @ Parametro In:	None
 * 	 --------- --
 * @ Parametro Retorno:	None
 *   --------- -------
 **********************************************************************/
static void LoadNibble(uint8_t Dato)
{
#ifndef _OrdenDescendente
	uint8_t i = PinB4;
	uint8_t j = 4;
		while(i<(PinB7+1))
			//while(i<=PinB7)
		{
			// para simplicidad ((Dato>>j)&0x01)?LPC_GPIO[PortBusDato].SET = (01<<i):LPC_GPIO[PortBusDato].CLR = (01<<i);
		 if(Dato&(01<<j)) LPC_GPIO[PortBusDato].SET |= (01<<i);
		 else LPC_GPIO[PortBusDato].CLR |= (01<<i);
		 j++;
		 i++;
		}
#else
		uint8_t i = PinB7;
		uint8_t j = 7;
		while(i<(PinB4+1))
		{
		 if(Dato&(01<<j)) LPC_GPIO[PortBusDato].SET| = (01<<i);
		 else LPC_GPIO[PortBusDato].CLR |= (01<<i);
		 j--;
		 i++;
		}
#endif
}

/*********************************************************************//**
 * @ Descripcion: Cargamos el Dato sobre el bus de datos del modulo LCD
 * 	 -----------
 * @ Formato Uso:  DatoLCD = [dato]; \ LoadDatoLCD();
 * 	 ------- ---
 *
 * @ Parametro In:	None
 * 	 --------- --
 * @ Parametro Retorno:	None
 *   --------- -------
 **********************************************************************/
void LoadDatoLCD(uint8_t Dato)
{
	LoadNibble(Dato);
	CicloEnable();
	Dato=Dato<<4;
	LoadNibble(Dato);
	CicloEnable();
}



void LoadHalfDataLCD(uint8_t Dato)
{
	LoadNibble(Dato);
	CicloEnable();
}




/*====================[external, functions definition]=====================*/

/*********************************************************************//**
 * @ Descripcion: Envio de Instruccion al Modulo LCD
 *
 * @ Parametro In:
 * 					#_ Instr -> Instruccion que deseamos enviar
 * 					al modulo LCD
 *
 * @ Parametro Retorno: 		None
 **********************************************************************/
static void SendInstruccionLCD(uint8_t Inst)
{
	__ConfigWriteInstLCD();
	LoadDatoLCD(Inst);
	DelayUs(72);
}

/*********************************************************************//**
 * @ Descripcion: Envio de un Dato al Modulo LCD
 *
 * @ Parametro In:
 * 					#_ Dato:	Dato a enviar
 *
 * @ Parametro Retorno: 		None
 **********************************************************************/
/*static void SendDataLCD(uint8_t Dato)
{
	__ConfigWriteDatoLCD();
	LoadDatoLCD(Dato);
	DelayUs(TIME_WRITE_DATA);
}*/


void ConfigBusDataAndControl(void)
{
	//-- Configuramos el Bus de Control
		__ConfigBusControlLCD();
	//-- Configuramos el Bus de Datos
		__ConfigBusDatoLCD();
}




/***********************************************************************
 * @ Descripcion: Inicializacion del modulo LCD
 * 				  #_ Inicializacion recomendada por Hoja de dato
 * 				  Para Bus de datos de 4-Bits, 2-Lineas,
 * 				  Fuente de Cracter 8x5 [dot]
 *
 * @ Parametro In:	None
 *
 * @ Parametro Retorno:	None
 **********************************************************************/
void InicioModuloLCD_WH1602C(void)
{
/*---------------[Configuration Port bus Instruction]---------------*/
	//__ConfigBusControlLCD();
/*-------------------[Configuration Port bus Data]------------------*/
	//__ConfigBusDatoLCD();
	Chip_IOCON_ConfigPinModeAndFunction(PortBusCtrl,PinE,NOT_PULL_MODE,PIN_FUNC_0);
	Chip_IOCON_ConfigPinModeAndFunction(PortBusCtrl,PinRS,NOT_PULL_MODE,PIN_FUNC_0);

	//ConfigBusDataAndControl();
	LPC_GPIO[PortBusCtrl].DIR |=((01<<PinE)|(01<<PinRS));// MaskBusCtrl;
	LPC_GPIO[PortBusCtrl].MASK &= (~MaskBusCtrl);

	//
	Chip_IOCON_ConfigPinModeAndFunction(PortBusDato,PinB4,NOT_PULL_MODE,PIN_FUNC_0);
	Chip_IOCON_ConfigPinModeAndFunction(PortBusDato,PinB5,NOT_PULL_MODE,PIN_FUNC_0);
	Chip_IOCON_ConfigPinModeAndFunction(PortBusDato,PinB6,NOT_PULL_MODE,PIN_FUNC_0);
	Chip_IOCON_ConfigPinModeAndFunction(PortBusDato,PinB7,NOT_PULL_MODE,PIN_FUNC_0);
	// -- Configuramos el Bus de Datos
	LPC_GPIO[PortBusDato].DIR |= MaskBusDato;
	LPC_GPIO[PortBusDato].MASK &=(~MaskBusDato);
	/* Function Set Formato: | 0 | 0 | 1 | DL | N | F | * | * |
		 *
		 * DL: Seleccion del Tipo de Interfaces
		 * 		DL = 0 -> Interfaces de 4-Bits
		 * 		DL = 1 -> Interfaces de 8-Bits
		 *
		 * N: Seleccion del Numero de Lineas del display
		 * 		N = 0 -> 1-Linea
		 * 		N = 1 -> 2-Lineas
		 *
		 * F : Seleccion de la Fuente 'font' para los caracteres
		 * 		F = 1 -> Fuente 5x8 puntos
		 * 		F = 0 -> Fuente 5x10 Pntos (solo valido cundo usamos una sola linea)
		 * 		No nos permite habilitar dos lienas con un font de 5x10 puntos.	 * */

		//==== Configuramos el Bus de Dato&Control Para el Dispositivo ====

		//-- Inicializacion de Modulo LCD
		DelayUs(20000);
		// RS & RW == 0
		__ConfigWriteInstLCD();
	// -- Cargamos la Primer instruccion
		// -- Instruccion A
		LoadHalfDataLCD(0b00110000);
		DelayUs(5000);
		// -- Instruccion B
		LoadHalfDataLCD(0b00110000);
		DelayUs(150);
		// -- Instruccion C
		LoadHalfDataLCD(0b00110000);
		DelayUs(150);
		// -- Instruccion D
		LoadHalfDataLCD(0b00100000); // Funcion SET
	// Comenzamos a cargar la Configuracin deseada
		// -- Configuramos el numero de Lineas de display y Font
		/*
		 * 0 0 1 DL N F - -
		 * DL: Longitud del Bus [1/0]=[8/4]
		 * N: Numero de Lineas  [1/0]=[2/1]
		 * F: Font [1/0] = [5x11 dots / 5x8 dots]
		 * */
		SendInstruccionLCD(0b00101000);
		// Display Off
		SendInstruccionLCD(INST_DISPLAY_OFF);
		// Display Clear
		SendInstruccionLCD(INST_CLEAR_DISP);
		DelayUs(1500);
		// -- Modo Set, 0000 0 1 I/D S
		SendInstruccionLCD(0b00000110);
		// -- Encendemos el Display
		SendInstruccionLCD(INST_DISPLAY_ON);
		// -- Volvemos el Cursor al Inicio
		SendInstruccionLCD(INST_CURSOR_HOME);
		DelayUs(1500);
	ptrCursor = 0;
	ptrDDRAM = 0;
	ptrWindows=0;
}



/***********************************************************************
 * @ Descripcion: Imprime un mensaje en la pantalla
 * 				  al Modulo LCD
 *
 * @ Parametro In:
 * 					#_ ptCadena -> Puntero a la cadena de caracters
 * 					o vector que contiene los string en formato
 * 					ASCII
 * 					#_ Longitud -> Longitud de la Cadena a enviar
 * 					Maxima Longitud 16-Caracteres
 *
 * @ Parametro Retorno: 		None
 **********************************************************************/
void PrintLCD_WH1602C(uint8_t Adr,unsigned char *ptrCadena,uint8_t Length)
{
	uint8_t i;
	/*if((Adr&0x3F)<32)
	{
		i = (Length<=16)? Length:16;
	}
	else
	{
		i = (Length<=8)? Length:8;
	}*/
	i = Length;

	unsigned char *ptrString = ptrCadena;
//-- Configuramos el Bus de Control y el Bus de Datos
	ConfigBusDataAndControl();
//
	//
//-- Seteamos la Direccion en memoria
	// -- Configuramos la escritura de Intruccion
	if(Adr!=NULL_ADR)
	{
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(Adr|0b10000000);
	DelayUs(TIME_INST_GENERIC);
	}
//-- Enviamos los Datos al modulo LCD
	//-- Configuramos la escritura de datos sobre el modulo
	__ConfigWriteDatoLCD();
	while(i)
	{
		if((*ptrString)=='\0')break;
		LoadDatoLCD((*(ptrString++)));
		DelayUs(TIME_WRITE_DATA);
		//ptrString++;
		i--;
	}
}

uint8_t GetPunteroLCD(enumTypePtr IdPtr)
{
	switch(IdPtr)
	{
	default:
	case PTR_DDRAM:
		return ptrDDRAM;
	case PTR_CURSOR:
		return ptrCursor;
	case PTR_WINDOWS:
		return ptrWindows;
	}
}


void ClearDisplay_WH1602C(void)
{
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(INST_CLEAR_DISP);
	DelayUs(TIME_INST_CLEAR);
}

void CursorHome_WH1602C(void)
{
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(INST_CURSOR_HOME);
	DelayUs(TIME_INST_RET_HOME);
	ptrCursor=0;
	ptrWindows=0;
}

void CursorOn_WH1602C(void)
{
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(statusDisplay|0b00001010);
	DelayUs(TIME_INST_GENERIC);
}

void CursorOff_WH1602C(void)
{
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(statusDisplay&0b00001101);
	DelayUs(TIME_INST_GENERIC);
}

void SetPosCursor_WH1602C(uint8_t posCursor)
{
	ptrCursor = (posCursor|0b10000000);
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(ptrCursor);
	DelayUs(TIME_INST_GENERIC);
	//-- Encendemos el Cursor
	LoadDatoLCD(statusDisplay|0b00001010);
	DelayUs(TIME_INST_GENERIC);
}

void BlinkOn_WH1602C(void)
{
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(statusDisplay|0b00001001);
	DelayUs(TIME_INST_GENERIC);
}

void BlinkOff_WH1602C(void)
{
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(statusDisplay&0b00001110);
	DelayUs(TIME_INST_GENERIC);
}

void SetPosBlink_WH1602C(uint8_t posBlink)
{
	ptrCursor = (posBlink|0b10000000);
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	//-- Cargamos la instruccion y Esperamos a que este se ejecute
	LoadDatoLCD(ptrCursor);
	DelayUs(TIME_INST_GENERIC);
	//-- Encendemos el Cursor
	LoadDatoLCD(statusDisplay|0b00001001);
	DelayUs(TIME_INST_GENERIC);
}

void ShiftLeftDisplay_WH1602C(void)
{
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	uint8_t i;
	switch(ptrWindows)
	{
	default:
	case 0:
		/* Windows 0 [0~15]*/
		i = 16;
		break;
	case 16:
		/* Windows 0 [0~15]*/
		i = 32;
		break;
	case 32:
		i=40;
		break;
	}
/**/
	while(ptrWindows<i)
	{
		LoadDatoLCD(INST_SHIFT_LEFT);
		DelayUs(TIME_INST_GENERIC);
		ptrWindows++;
	}
	if(ptrWindows==40)ptrWindows =0;
}

void ShiftRigntDisplay_WH1602C(void)
{
	ConfigBusDataAndControl();
	__ConfigWriteInstLCD();
	uint8_t i;
	switch(ptrWindows)
	{
	default:
	case 32:
		/* Windows 0 [0~15]*/
		i = 16;
		break;
	case 16:
		/* Windows 0 [0~15]*/
		i = 0;
		break;
	case 0:
		i=32;
		ptrWindows = 40;
		break;
	}
/**/
	while(ptrWindows>i)
	{
		LoadDatoLCD(INST_SHIFT_RIGHT);
		DelayUs(TIME_INST_GENERIC);
		ptrWindows--;
	}
}

/*==============================[end of file]==============================*/


