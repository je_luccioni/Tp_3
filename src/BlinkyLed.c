#define __BlinkyLed_c__
/*==================[inclusions]=============================================*/
#include <chip_lpc175x_6x.h>
#include <SysTickTimer.h>
#include <BlinkyLed.h>



/** \addtogroup BlinkyLed_h BlinkyLed module
 ** @{ */
/********************************[Header File]*************************************//**
 * @file    BlinkyLed.c
 * @brief	Archivo que contine las Declaracion de APIs del modulo
 * BlinkyLed, Inicializacion, Configuracion, y llamado recurrente
 * @version v01.01
 * @date   12/3/2016
 * @note none
 * @par  jel
 *************************************************************************************/
/*============[internal data declaration]===============================*/

/*============[internal functions declaration]===============================*/
/*******************************************************************//**
 * @brief	Actualiza el Periodo de Parpadeo del Led, considerando
 * el una relacion del 50%
 * @param 	Period, periodo deseado
 * @return	None
 *********************************************************************/
static void UpdatePeriodBlinkyLed(unsigned int Period);


#ifdef DUTY_CICLE_ON
/*******************************************************************//**
 * @brief	Actuiza el Duty Cicle de la Funcion Blinky Led.
 * @param 	Period, Perido del Blink
 * @param 	DutyCicle, valor del Ciclo Trabajo (Duty Cicle) deseado
 * en unidades de % [1~99]
 * @return	None
 *********************************************************************/
static void UpdateDutyCicleBlinkyLed(unsigned int Period,unsigned int DutyCicle);
#endif
/** @} doxygen end group definition */



/********************************************************************************
* @ Descripcion:	Configuracion y asignacion de los Pines al GPIO
* 					Inicializacion por defecto de las varibles
*
* @ Parametro In:	No Aplica
*
* @ return:	No Aplica
*
* @ HeaderFile: "BlinkLed.h"
*******************************************************************************/
void IniBlinkyLed(void)
{
	/* Configuramos el Pin como Salida */
	//PortLedBlk->FIODIR |= maskLed;
	/* Configurmaos la Mascara del GPIO*/
	//PortLedBlk->FIOMASK &= (~maskLed);
	/* Ponemos en Cero el GPIO */
	//PortLed->FIOCLR |= maskLed;
	/* Cargamos por Defecto la Variable Toff Ton*/

	Chip_GPIO_SetOutputPinDir(PortLedBlk,PinLedBlk);
	AcuMsBlk = GetAcuMsSysTick();
	AcuMsUpdateLed = GetAcuMsSysTick();
#ifdef DUTY_CICLE_ON
	UpdateDutyCicleBlinkyLed(1000,30);
#else
	UpdatePeriodBlinkyLed(1500);
#endif
}



/********************************************************************************
* @ Descripcion:	Actualiza el Periodo de Parpadeo del Led, considerando el
* 					una relacion del 50%
*
* @ Parametro In:	unsigned int Period
*
* @ return:	No Aplica
*
* @ Ejemplo: UpdatePeriodBlinkyLed(1000)
* 				Ton = 500; Toff = 500;
*******************************************************************************/
static void UpdatePeriodBlinkyLed(unsigned int Period)
{
#ifdef DUTY_CICLE_ON
	UpdateDutyCicleBlinkyLed(Period,(((Ton*100)/(Ton+Toff))));
#else //DUTY_CICLE_ON
	Ton = Period/2;
	Toff=Ton;
#endif//DUTY_CICLE_ON

}

#ifdef DUTY_CICLE_ON
/********************************************************************************
 * @ Descripcion	Actuiza el Duty Cicle de la Funcion Blinky Led.
 *
 * @ Parametro In 	Period, Perido del Blink
 *
 * @ Parametro In 	DutyCicle, valor del Ciclo Trabajo (Duty Cicle) deseado
 * en unidades de % [1~99]
 *
 * @ return	None
 * @ HeaderFile: "BlinkLed.h"
 *******************************************************************************/
static void UpdateDutyCicleBlinkyLed(unsigned int Period,unsigned int DutyCicle)
{
	Ton = (DutyCicle*Period)/100;
	Toff = Period-Ton;
}
#endif//


/********************************************************************************
* @ Descripcion:	Funcion Central del modulo, la cual se encarga de verificar
* 					y realizar los cambios del led en funcion del tiempo.
* 					Invocada desde el programa principal (dentro del Lazo ppa)
*
* @ Parametro In:	No Aplica
*
* @ return:	No Aplica
*
* @ HeaderFile: "BlinkLed.h"
*******************************************************************************/
void BlinkyLed(void)
{
	unsigned int aux;
	Chip_GPIO_SetPinMask(PortLedBlk,PinLedBlk);
	aux=(Chip_GPIO_GetPinValue(PortLedBlk,PinLedBlk))?Ton:Toff;
	//PortLedBlk->FIOMASK &= (~maskLed);
	//((PortLedBlk->FIOPIN) & maskLed)? aux=Ton : aux= Toff;
	//if((PortLedBlk->FIOPIN) & maskLed) aux = Ton;
	//aux = Toff;
	if(ConsultTime(aux,&AcuMsBlk)==FALSE)return;
	Chip_GPIO_TogglePinState(PortLedBlk,PinLedBlk);
	//Chip_GPIO_TooglePinState(0,22,DISABLE_MASK);
		//PortLedBlk->FIOPIN ^= maskLed;
}


/********************************************************************************
* @ Descripcion:	Funcion que actualiza autoamticamente el periodo del Blink
* 					Cad 5 segundos, con incemento/decremento, correspondiente
* 					DELTA_TIME
* 					Invocada desde el Programa principal
*
* @ Parametro In:	No Aplica
*
* @ return:	No Aplica
*
* @ Ejemplo: UpdateBlinkyLed();
*
* @ HeaderFile: "BlinkLed.h"
*******************************************************************************/
void UpdateBlinkyLed(void)
{
	static unsigned char Status;
	unsigned int Period;

	if(!(ConsultTime(5000,&AcuMsUpdateLed)))return;
	Period = Ton+Toff;
	switch(Status)
	{
	case 0:
		AcuMsUpdateLed = GetAcuMsSysTick();
		UpdatePeriodBlinkyLed(1500);
		Status++;
		return;

	case 1:
		(Period<PERIOD_MAX)? UpdatePeriodBlinkyLed((Period+DELTA_TIME)):Status++;
		return;

	case 2:
		(Period>PERIOD_MIN)? UpdatePeriodBlinkyLed((Period-DELTA_TIME)):Status--;
		return;

	default:
		Status=0;
		return;
	}
}
