#define __DriverLed_c__
/*==================[inclusions]=============================================*/

#include <chip_lpc175x_6x.h>
#include <SysTickTimer.h>
#include <DriverLed.h>
#include <DriverEEPROM24LC64.h>
/** \addtogroup DriverLed_h DriverLed module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    DriverLed.c
 * @brief	Archivo que contine las Declaracion de APIs del modulo DriverLed
 * Configuracion (GPIO), Inicializacion (LEDs), y Cambio de Estados (On/Off) del Led
 * Deseado
 * @version v01.01
 * @date   12/3/2016
 * @note none
 * @par  jel
 *************************************************************************************/

/*===================[internal macros]=======================================*/
#define PinLed1		12		/**<@brief Asignacion del Pin al LED1 */
#define PinLed2		11		/**<@brief Asignacion del Pin al LED2 */
#define PinLed3		4		/**<@brief Asignacion del Pin al LED3 */
#define PinLed4		3		/**<@brief Asignacion del Pin al LED4 */

#define PortLed1	2	/**<@brief Asignacion del Puerto al LED1 */
#define PortLed2	2	/**<@brief Asignacion del Puerto al LED2 */
#define PortLed3	2	/**<@brief Asignacion del Puerto al LED3 */
#define PortLed4	2	/**<@brief Asignacion del Puerto al LED4 */
//#define DIFF_PORT		/**<@brief Definimos si los PortLed son difernetes */
	const unsigned char vctPinLed[]={PinLed1,PinLed2,PinLed3,PinLed4};
	const unsigned char vctPortLed[]={PortLed1,PortLed2,PortLed3,PortLed4};
/*==================[internal data definition]==============================*/



/*==================[internal functions definition]=========================*/

/*==================[internal data definition]===============================*/

/** @} doxygen end group definition */


/*==================[internal data declaration]==============================*/

/*==================[external data definition]===============================*/





/*==================[external functions definition]==========================*/


/********************************************************************************
* @ Descripcion:	Api pra la inicializacion de los GPIO relacionado a los LEDs
*
* @ Parametro In	None
*
* @ return			None
*
* @ HeaderFile: DriverLed.c
*******************************************************************************/
void IniDriverLed(void)
{
	unsigned char i = 0;
	unsigned int saveAux;
	ReadEEPROM24LC64(ADR_EEPROM24LC64_1,0x0000,&saveAux,sizeof(saveAux));
	if(!((saveAux>=300)&&(saveAux<=3000)))
	{
		saveAux= 500;
	}
	/*
	TimeLed1 = saveAux;
	TimeLed2 =saveAux;
	TimeLed3 = saveAux;
	TimeLed4 = saveAux;
	else {	TimeLed1 = TimeLed2 = TimeLed3 = TimeLed4 = 500;}
	AcuMsLed1=AcuMsLed2=AcuMsLed3=AcuMsLed4=0;*/
	while(i<4)
	{
		vctTimeLed[i]=saveAux;
		vctAcuMsLed[i]=0;
		/* Configuramos todos los pines, modo y funcion*/
		 //Chip_IOCON_ConfigPin(vctPortLed[i],vctPinLed[i],NOT_PULL_FUNC0);

		 Chip_IOCON_ConfigPinModeAndFunction(vctPortLed[i],vctPinLed[i],NOT_PULL_MODE,PIN_FUNC_0);
		/* configuramos el open drain del los pines */
		 Chip_IOCON_EnableOpenDrain(vctPortLed[i],vctPinLed[i]);
		/* Configuramos el Pin como Salida */
		 Chip_GPIO_SetOutputPinDir(vctPortLed[i],vctPinLed[i]);
		/* Habilitamos la mascara*/
		/* Apagamos los Leds, nos aseguramos*/
		 Chip_GPIO_SetPinState(vctPortLed[i],vctPinLed[i]);
		i++;
	}
}

/********************************************************************************
* @ Descripcion:	Funcion que realiza cambio de Estado (On/Off) sobre un
* Led especifico
*
* @ Parametro In	IdLed, Identificacion del Led sobre el cual se desea
* realizar el cambio de estado, puede adoptar los siguentes valores
* 					+ TOOGLE_LED1
* 					+ TOOGLE_LED2
* 					+ TOOGLE_LED3
* 					+ TOOGLE_LED4
*
* @ return:		None
*
* @ HeaderFile: DriverLed.h
*******************************************************************************/
void ToggleLed(enumTypeToggleLed IdLed)
{
	Chip_GPIO_SetPinMask(vctPortLed[IdLed],(vctPinLed[IdLed]));
	Chip_GPIO_TogglePinState(vctPortLed[IdLed],(vctPinLed[IdLed]));
}



void ParpadeoLed(enumTypeToggleLed IdLed)
{
	/* Porque de esta forma, ya que lo podemos hacer mediante vectores
	 * */

	if(ConsultTime(vctTimeLed[IdLed],&(vctAcuMsLed[IdLed])))
	{
		ToggleLed(IdLed);
		return;
	}
	else return;
	/*
	switch(IdLed)
	{
	default:
		return;
	case TOGGLE_LED1:
		if(ConsultTime(TimeLed1,&AcuMsLed1))
		{
			ToggleLed(TOGGLE_LED1);
			return;
		}
		else return;
	case TOGGLE_LED2:
		if(ConsultTime(TimeLed2,&AcuMsLed2))
		{
			ToggleLed(TOGGLE_LED2);
			return;
		}
		else return;
	case TOGGLE_LED3:
		if(ConsultTime(TimeLed3,&AcuMsLed3))
		{
			ToggleLed(TOGGLE_LED3);
			return;
		}
		else return;
	case TOGGLE_LED4:
		if(ConsultTime(TimeLed4,&AcuMsLed4))
		{
			ToggleLed(TOGGLE_LED4);
			return;
		}
		else return;

	}*/



}


void UpdateTimeParpadeo(enumTypeToggleLed IdLed,unsigned int argTime)
{
	vctTimeLed[IdLed] = argTime;
	/*
	switch(IdLed)
	{
	default:
		return;
	case TOGGLE_LED1:
		TimeLed1=argTime;
		break;
	case TOGGLE_LED2:
		TimeLed2=argTime;
		break;
	case TOGGLE_LED3:
		TimeLed3=argTime;
		break;
	case TOGGLE_LED4:
		TimeLed4=argTime;
		break;

	}*/
}

unsigned int GetTimeParpadeo(enumTypeToggleLed IdLed)
{
	return(vctTimeLed[IdLed]);
	/*
	switch(IdLed)
	{
	default:
	case TOGGLE_LED1:
		return TimeLed1;
	case TOGGLE_LED2:
		return TimeLed2;
	case TOGGLE_LED3:
		return TimeLed3;
	case TOGGLE_LED4:
		return TimeLed4;
	}*/

}

/*==================[internal functions declaration]=========================*/




/*==================[end of file]============================================*/
