#define __DriverEEPROM24LC64_c__
/*==================[inclusions]=============================================*/
//#include <chip_lpc175x_6x.h>
#include <DriverEEPROM24LC64.h>


/** \addtogroup DriverEEPROM24LC16_c  DriverEEPROM24LC16 module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    DriverEEPROM24LC16.c
 * @brief	Archivo que contine las Definiciones de las APIs del modulo DriverEEPROM24LC16
 * <descripcionBreve>
 * @version v0x.0y
 * @date   16/5/2016
 * @note none
 * @par  jel
 *************************************************************************************/


/** \addtogroup  DriverEEPROM24LC16_c  DriverEEPROM24LC16 module
 ** @{ */ /*en caso de necesitar documentar las definiciones internas*/

/*==============================[]==============================*/

/*============================[internal macros]============================*/
#define WAIT_DUMMY		0xFFFF
#define SIZE_PAGE		32
#define NUM_REENTRY		5
/*=====================[internal, typedef definition]======================*/



/*=======================[internal data declaration]=======================*/


/*====================[internal, functions definition]=====================*/




/** @} doxygen end group definition */






/*====================[external, functions definition]=====================*/

/* 24LC64 -> Memoria EEPROM de 64K byte
 * Direccionamiento de 16-Bits 2^6 K = 2^6 . 2^10
 *
 * */
void IniDriverEEPROM24LC64(void)
{
	// -> Configuramos los Pines del I2C destinado
	Chip_IOCON_ConfigPinModeAndFunction(PortSDA,PinSDA,NOT_PULL_MODE,FUNCTION_PIN);
	Chip_IOCON_ConfigPinModeAndFunction(PortSCL,PinSCL,NOT_PULL_MODE,FUNCTION_PIN);
	switch (I2C_EEPROM24LC64)
	{
		default: /* Por defecto configura el I2C0*/
		case I2C0:
			Chip_IOCON_ConfigI2C0(I2C0_MODE_STANDARD);
			break;

		case I2C1:
		case I2C2:
			Chip_IOCON_EnableOpenDrain(PortSDA,PinSDA);
			Chip_IOCON_EnableOpenDrain(PortSCL,PinSCL);
			break;
		/* -- En caso de poseer mas de tres perifericos I2C, debemos colocar las configuracones aqui debajo*/
	}

	Chip_I2C_SetClockRate (I2C_EEPROM24LC64, 100000);
	Chip_I2C_SetMasterEventHandler(I2C_EEPROM24LC64,Chip_I2C_EventHandlerPolling);
	Chip_I2C_Init(I2C_EEPROM24LC64);

}

static void WaitDummy(void)
{
	unsigned int i;
	for(i=0;i<WAIT_DUMMY;i++){}
}


/*********************************************************************
 * @brief	Write a block of data to Memory EEPROM, connect to Bus I2C
 * @param IdDevice	: Identification of the address Memory, option
 * 	+ ADR_EEPROM24LC64_1, Address Memory Slave 1 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_2, Address Memory Slave 2 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_3, Address Memory Slave 3 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_4, Address Memory Slave 4 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_5, Address Memory Slave 5 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_6, Address Memory Slave 6 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_7, Address Memory Slave 7 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_8, Address Memory Slave 8 for EEPROM24LC64
 * @param MemPosition	: Memory Position, inside memory
 * @param *ptrDatoW		: Punter to data send/write
 * @param Length		: length of the block to write [sizeof(nmbBlock)]
 * @return
 * 	+ TRANSFER_SUCCESS 	: Transfer success Length = Byte Send
 * 	+ TRANSFER_ERROR 	: Transfer error Length <> byte send
 *********************************************************************/
uint16_t WriteEEPROM24LC64(enumTypeIdDevice IdDevice, uint16_t MemPosition,void *ptrDatoW, uint16_t Length)
{
	// -- Definimos todas las variables de alcance local
	uint8_t i;
	uint8_t saveBuffer[34];
	uint16_t Limit;
	uint16_t Cantidad;
	uint16_t count;
	uint16_t countTotal,Reentry;
	/*declaro e inicializo a cero todos los elementos de la estrucutura*/
	I2C_XFER_T  cfgTranfI2C;
	//
	countTotal = 0;
	Reentry = ((Length/32)+1)*(NUM_REENTRY);
// -- Lazo de Envio por paquetes
lblSend:
	Limit =( ((uint16_t) (MemPosition/SIZE_PAGE)+1)*(SIZE_PAGE))-MemPosition;
	if(Length>Limit) Cantidad=((uint8_t) Limit) + 2;
	else Cantidad = Length +2;
	/* Definimos un buffer, contemplando que el mismo contendra la Direccion de Memoria */
	/* ojo con esta declaracion, se redimencionara al tamaño del primer llamado, luego este
	 * permanecera de dicho tamaño
	 unsigned char saveBuffer[Length+2];*/

	/*Seteamos la Direccion de memoria*/
	 saveBuffer[0]=(uint8_t)((MemPosition&0xFF00)>>8); //Parte alta de la dirección
	 saveBuffer[1]=(uint8_t)(MemPosition&0x00FF); //Parte baja de la dirección

	 for(i=2;i<Cantidad;i++)
	 {
		 saveBuffer[i]=  (*((uint8_t*) ptrDatoW++));/*Asigno y luego incrementa el puntero*/
	 }

	/* Configuramos la Direccion Slave de I2C*/
	cfgTranfI2C.slaveAddr =IdDevice;
	/* Seteamos el Tamño y la direccion del Buffer de Tx */
	cfgTranfI2C.txSz= Cantidad;
	cfgTranfI2C.txBuff=saveBuffer;
	/* Seteamos el Tamaño y la Direecion del Buffer de Rx */
	cfgTranfI2C.rxSz=0;
	cfgTranfI2C.rxBuff = NULL;
	/* Inicializimos el Registro de Status */
	cfgTranfI2C.status=I2C_STATUS_DONE;
	//
	/* 	I2C_STATUS_DONE		-> Transfer done successfully
	 * 	I2C_STATUS_NAK		-> NAK received during transfer
	 * 	I2C_STATUS_ARBLOST	-> Aribitration lost during transfer
	 * 	I2C_STATUS_BUSERR	-> Bus error in I2C transfer
	 * 	I2C_STATUS_BUSY		-> I2C is busy doing transfer */
	if(Chip_I2C_MasterTransfer(I2C_EEPROM24LC64,&cfgTranfI2C)==I2C_STATUS_DONE)
	{
		count = Cantidad -2 ;// count tendra la cantidad envida
	}
	else {count = Cantidad - cfgTranfI2C.txSz;}
	countTotal+=count;
	WaitDummy();
	if (count != Length+2)
	{
	 // debemos reenviar
	  MemPosition += count;
	  Length -= count;
	  if(--Reentry) goto lblSend;
	  return countTotal;
		// devemos contemplar los reintentos, para no caer en un lazo infinito
	 }
	return countTotal;

		/*
		 *  Consifderaciones para REENVIO, para salir del Lazo
		 *
		 *  Reentry = ((Length/32)+1)*(NUM_REENTRY);
		 *
		 * */
}

/*********************************************************************
 * @brief	Read a block of Memory from EEPROM, connect to Bus I2C
 * @param IdDevice	: Identification of the address Memory, option
 * 	+ ADR_EEPROM24LC64_1, Address Memory Slave 1 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_2, Address Memory Slave 2 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_3, Address Memory Slave 3 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_4, Address Memory Slave 4 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_5, Address Memory Slave 5 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_6, Address Memory Slave 6 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_7, Address Memory Slave 7 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_8, Address Memory Slave 8 for EEPROM24LC64
 * @param MemPosition	: Memory Position, inside memory
 * @param *ptrDatoW		: Punter to buffer of data receiver
 * @param Length		: length of the block to read[sizeof(nmbBlock)]
 * @return
 * 	+ TRANSFER_SUCCESS 	: Transfer success Length = Byte Send
 * 	+ TRANSFER_ERROR 	: Transfer error Length <> byte send
 *********************************************************************/
enumTypeTransfer ReadEEPROM24LC64(enumTypeIdDevice IdDevice, uint16_t MemPosition,void *ptrDatoR, uint16_t Length)
{
	//
	unsigned char bufferTx[2];
	uint16_t AdrEEPROM;
	I2C_XFER_T  cfgTranfI2C;//={0};
	//unsigned char *ptrDato;
	//

	AdrEEPROM = MemPosition;
	/*Seteamos la Direccion de memoria*/
	bufferTx[0]=(unsigned char)((AdrEEPROM&0xFF00)>>8); //Parte alta de la dirección
	bufferTx[1]=(unsigned char)(AdrEEPROM&0x00FF); //Parte baja de la dirección
	/* Configuramos la Direccion Slave de I2C*/
	cfgTranfI2C.slaveAddr =IdDevice;
	/* Seteamos el Tamño y la direccion del Buffer de Tx */
	cfgTranfI2C.txSz=2;//sizeof(bufferTx);
	cfgTranfI2C.txBuff=bufferTx;
	/* Seteamos el Tamaño y la Direecion del Buffer de Rx */
	cfgTranfI2C.rxSz= Length;
	cfgTranfI2C.rxBuff =(uint8_t *) ptrDatoR;
	/* Inicializimos el Registro de Status */
	cfgTranfI2C.status=I2C_STATUS_DONE;
	//
	if(Chip_I2C_MasterTransfer(I2C_EEPROM24LC64,&cfgTranfI2C)==I2C_STATUS_DONE)
	{
		return Length ;// count tendra la cantidad envida
	}
	else  return (Length-cfgTranfI2C.rxSz);
}





/*==============================[end of file]==============================*/


