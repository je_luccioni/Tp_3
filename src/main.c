/* Autor: Luccioni
 * Date
 **/

/** @brief Trabajo Integrador Numero 1, cinta Transportadora.
 */

/*==================[inclusions]=============================================*/
#include <chip_lpc175x_6x.h>
#include <FSM_Pulsador.h>/* Agregamos el Archivo de cavecera que contiene
							los prototipos de las Funciones de la FSM */
#include <BlinkyLed.h>
#include <SysTickTimer.h>
#include <DriverLed.h>
#include <DriverEEPROM24LC64.h>
#include <DriverWH1602C_2C4D.h>



/** \addtogroup main_h main module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    main.h
 * @brief	Archivo que contine las Declaraciones de APIs del modulo main
 * Técnicas Digitales II - Guía de Trabajos Prácticos Ejercicio TP2B
 * @version v01.01
 * @date   12/3/2016
 * @note none
 * @par  Rigalli, Baffico, Luccioni
 *************************************************************************************/

/*==================[macros and definitions]=================================*/
#define PortSWn		0
//
#define PinSWn		1


#define CONFIG_HARDWARE_SWn();	{\
/*========================[Configuracion de los Pines, como Entradas]========================*/\
	Chip_IOCON_ConfigPinModeAndFunction(PortSWn,PinSWn,PULL_UP_MODE,PIN_FUNC_0);\
	/* -- Configramos los GPIO, como Entrada */\
	Chip_GPIO_SetInputPinDir(PortSWn,PinSWn); \
}

#define BASEmEMORYeEPROM_MSJ1	50



/*==================[internal data declaration]==============================*/
typedef struct
{
	unsigned char MsjScr1[16];
	unsigned char MsjScr2[16];
	unsigned char MsjScr3[8];
	//uint8_t AdrDDRAM[1];
	//uint16_t AdrEEPROM;
}strTypeArrayLCDFormat1;
typedef struct
{
	unsigned char MsjWin1AndWin2[32];
	unsigned char MsjWin3[8];
	//uint8_t AdrDDRAM[1];
	//uint16_t AdrEEPROM;
}strTypeArrayLCDFormat2;

//strTypeArrayMemLCD Array[] ={ (uint8_t*) "Unsigend mesnaje",(LINE1),1024 };




strTypeArrayLCDFormat2 ArrayMsj2[] = {\
	/*    0123456789ABCDEF0123456789ABCDEF - 01234567 */\
		{"Vienvenido Mundo Pantalla Dos...","Ventana3"},
		{"Autor: Luccioni, Jesus Emanuel..","Fin....."}
};
	 // WriteEEPROM24LC64(ADR_EEPROM24LC64_1,64,(uint8_t*)"Luccioni Jesus E",sizeof("Luccioni Jesus E"));


/*==================[internal functions declaration]=========================*/
/*******************************************************************//**
 * @brief	Function for initialization of peripheral
 * @param 	None
 * @return None
 *********************************************************************/
static void initHardware(void);
/*******************************************************************//**
 * @brief Function for initialization of software object *
 * @param None
 * @return None
 *********************************************************************/
static void iniSoftware(void);

/*******************************************************************//**
 * @brief	update the event SysTick and runCicle of the StateChar
 * @param None
 * @return None
 *********************************************************************/
static void UpdateFSM(void);

/*******************************************************************//**
 * @brief	Read the status of SWn and call correspond function
 * @param None
 * @return None
 *********************************************************************/
static void ReadSWn(void);

static void WriteInMemoryEeprom(void);
static void ReadAndPrintMsj(void);

/*==================[internal data definition]===============================*/

/** @} doxygen end group definition */


/** statechart instance */
FSM_Pulsador hdlFsmPulsador0; /* Definimos una varible correspondiente al
 	 	 	 	 	 	 	 	 al handler de la Maquina de estado
 	 	 	 	 	 	 	 	 con esta podremos identificar la maquina de
 	 	 	 	 	 	 	 	 estados, cuando se haga uso de las APIS generadas */


/*==================[external functions declaration]===============================*/

/* Accion a Realizar cuando se Presione El Pulsador.
 * El arguemento Handler es utilizado para  recuerencia de la FSM.
 * */
void fSM_PulsadorIface_aPulsado(FSM_Pulsador* handle)
{
	unsigned int saveTime;
	saveTime=GetTimeParpadeo(TOGGLE_LED1);
	switch (saveTime)
	{
	case 300:
		saveTime=500;
		break;
	case 500:
		saveTime=1300;
		break;
	case 1300:
		saveTime=3000;
		break;
		// -- establecemos el Ultimo caso como default
	default:
	case 3000:
		saveTime=300;
		break;
	}
	UpdateTimeParpadeo(TOGGLE_LED1,saveTime);
	WriteEEPROM24LC64(ADR_EEPROM24LC64_1,0x0000,&saveTime,sizeof(saveTime));
	ShiftLeftDisplay_WH1602C();
}

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/




/*==================[internal functions definition]==========================*/

static void iniSoftware(void)
{
	/*! Initializes the StatechartSWn state machine data structures.
	 *  Must be called before first usage.*/
	fSM_Pulsador_init(&hdlFsmPulsador0);
	/*! Activates the state machine */
	fSM_Pulsador_enter(&hdlFsmPulsador0);
}

static void initHardware(void)
{
	InicioModuloLCD_WH1602C();
	/* Habilitamos el Clock Periferico p/GPIOs */
	Chip_GPIO_EnableClock();
	IniDriverEEPROM24LC64();
	ConfigSysTick();
	IniBlinkyLed();
	IniDriverLed();
	CONFIG_HARDWARE_SWn();

}


/*==================[external functions definition]==========================*/
int main(void)
{
	iniSoftware();
	initHardware();
	//
	WriteInMemoryEeprom();
	ReadAndPrintMsj();
	CursorHome_WH1602C();
	while (1)
	{
		BlinkyLed();
		UpdateBlinkyLed();
		ParpadeoLed(TOGGLE_LED1);
		UpdateFSM();
		ReadSWn();
	}
}

/*==================[internal functions definition]==========================*/
// -- Esta Funcion Prevalece de una maquina a otra,
/*Nota:
 * la finalidad de esta funcion o procedimiento es evitar latencia en las
 * Interupciones, no tanto por el evento del SysTick, si no por
 * el llamado a runCicle, medinate la cual se actualizan todos los estados
 * de la statechar */
static void UpdateFSM(void)
{
	static unsigned int Acu;//=GetAcuMsSysTick;
	if(ConsultTime(1, &Acu))
	{
		/* Agregamos el llamado a la api de Refresco de Registro de Eventos de la
		 * maquina de estado*/
		fSM_PulsadorIface_raise_eSysTick(&hdlFsmPulsador0);
		fSM_Pulsador_runCycle(&hdlFsmPulsador0);
	}
}

/*==================[internal functions definition]==========================*/
static void ReadSWn(void)
{
/* -- Leemos el Pulsador,con Pull-Up */
	if(Chip_GPIO_GetPinValue(PortSWn,PinSWn))
	// -- Detectamos un Uno en el Pin -> Pull
		fSM_PulsadorIface_raise_ePull(&hdlFsmPulsador0);
	else
		// -- Detectamos un Cero en el Pin -> Push
		fSM_PulsadorIface_raise_ePush(&hdlFsmPulsador0);
}




static void WriteInMemoryEeprom(void)
{
	strTypeArrayLCDFormat1 ArrayMsj[] = {\
		//    0123456789ABCDEF - 0123456789ABCDEF - 01234567
			{"Vienvenido Mundo","Pantalla Dos....","Ventana3"},
			{"Autor: Luccioni,","Jesus Emanuel...","Fin....."}
	};
	uint16_t AdrEEPROM;
	AdrEEPROM = BASEmEMORYeEPROM_MSJ1;
	WriteEEPROM24LC64(ADR_EEPROM24LC64_1,AdrEEPROM,&ArrayMsj,sizeof(ArrayMsj));
}

static void ReadAndPrintMsj(void)
{
	uint8_t bufferSave[20];//={0};
	uint16_t AdrEEPROM;
	uint8_t i;
	i=0;
	AdrEEPROM=BASEmEMORYeEPROM_MSJ1;
	while(i<4)
	{
	 AdrEEPROM +=ReadEEPROM24LC64(ADR_EEPROM24LC64_1,AdrEEPROM,bufferSave,20);
	 if(i==0) PrintLCD_WH1602C(LINE1,bufferSave,20);
	 else PrintLCD_WH1602C(NULL_ADR,bufferSave,20);
	 i++;
	}
}

/*==================[end of file]============================================*/












