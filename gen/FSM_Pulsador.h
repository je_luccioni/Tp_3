
#ifndef FSM_PULSADOR_H_
#define FSM_PULSADOR_H_

#include "sc_types.h"

#ifdef __cplusplus
extern "C" { 
#endif 

/*! \file Header of the state machine 'FSM_Pulsador'.
*/

/*! Enumeration of all states */ 
typedef enum {
	FSM_Pulsador_main_region_St0,
	FSM_Pulsador_main_region_St3,
	FSM_Pulsador_main_region_Wait,
	FSM_Pulsador_last_state
} FSM_PulsadorStates;

/*! Type definition of the data structure for the FSM_PulsadorInternal interface scope. */
typedef struct {
	sc_integer vAcuMs;
	sc_integer vFlg;
} FSM_PulsadorInternal;

/*! Type definition of the data structure for the FSM_PulsadorIface interface scope. */
typedef struct {
	sc_boolean ePush_raised;
	sc_boolean ePull_raised;
	sc_boolean eSysTick_raised;
} FSM_PulsadorIface;


/*! Define dimension of the state configuration vector for orthogonal states. */
#define FSM_PULSADOR_MAX_ORTHOGONAL_STATES 1

/*! 
 * Type definition of the data structure for the FSM_Pulsador state machine.
 * This data structure has to be allocated by the client code. 
 */
typedef struct {
	FSM_PulsadorStates stateConfVector[FSM_PULSADOR_MAX_ORTHOGONAL_STATES];
	sc_ushort stateConfVectorPosition; 
	
	FSM_PulsadorInternal internal;
	FSM_PulsadorIface iface;
} FSM_Pulsador;

/*! Initializes the FSM_Pulsador state machine data structures. Must be called before first usage.*/
extern void fSM_Pulsador_init(FSM_Pulsador* handle);

/*! Activates the state machine */
extern void fSM_Pulsador_enter(FSM_Pulsador* handle);

/*! Deactivates the state machine */
extern void fSM_Pulsador_exit(FSM_Pulsador* handle);

/*! Performs a 'run to completion' step. */
extern void fSM_Pulsador_runCycle(FSM_Pulsador* handle);


/*! Raises the in event 'ePush' that is defined in the default interface scope. */ 
extern void fSM_PulsadorIface_raise_ePush(FSM_Pulsador* handle);

/*! Raises the in event 'ePull' that is defined in the default interface scope. */ 
extern void fSM_PulsadorIface_raise_ePull(FSM_Pulsador* handle);

/*! Raises the in event 'eSysTick' that is defined in the default interface scope. */ 
extern void fSM_PulsadorIface_raise_eSysTick(FSM_Pulsador* handle);


/*!
 * Checks if the statemachine is active (until 2.4.1 this method was used for states).
 * A statemachine is active if it was entered. It is inactive if it has not been entered at all or if it was exited.
 */
extern sc_boolean fSM_Pulsador_isActive(const FSM_Pulsador* handle);

/*!
 * Checks if all active states are final. 
 * If there are no active states then the statemachine is considered as inactive and this method returns false.
 */
extern sc_boolean fSM_Pulsador_isFinal(const FSM_Pulsador* handle);

/*! Checks if the specified state is active (until 2.4.1 the used method for states was calles isActive()). */
extern sc_boolean fSM_Pulsador_isStateActive(const FSM_Pulsador* handle, FSM_PulsadorStates state);

#ifdef __cplusplus
}
#endif 

#endif /* FSM_PULSADOR_H_ */
