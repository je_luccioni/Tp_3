
#ifndef FSM_PULSADORREQUIRED_H_
#define FSM_PULSADORREQUIRED_H_

#include "sc_types.h"
#include "FSM_Pulsador.h"

#ifdef __cplusplus
extern "C" {
#endif 

/*! \file This header defines prototypes for all functions that are required by the state machine implementation.

This state machine makes use of operations declared in the state machines interface or internal scopes. Thus the function prototypes:
	- fSM_PulsadorIface_aPulsado
are defined.

These functions will be called during a 'run to completion step' (runCycle) of the statechart. 
There are some constraints that have to be considered for the implementation of these functions:
	- never call the statechart API functions from within these functions.
	- make sure that the execution time is as short as possible.
 
*/

extern void fSM_PulsadorIface_aPulsado(FSM_Pulsador* handle);


#ifdef __cplusplus
}
#endif 

#endif /* FSM_PULSADORREQUIRED_H_ */
