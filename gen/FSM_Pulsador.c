
#include <stdlib.h>
#include <string.h>
#include "sc_types.h"
#include "FSM_Pulsador.h"
#include "FSM_PulsadorRequired.h"
/*! \file Implementation of the state machine 'FSM_Pulsador'
*/

/* prototypes of all internal functions */
static sc_boolean fSM_Pulsador_check_main_region_St0_tr0_tr0(const FSM_Pulsador* handle);
static sc_boolean fSM_Pulsador_check_main_region_St3_tr0_tr0(const FSM_Pulsador* handle);
static sc_boolean fSM_Pulsador_check_main_region_Wait_tr0_tr0(const FSM_Pulsador* handle);
static sc_boolean fSM_Pulsador_check_main_region_Wait_tr1_tr1(const FSM_Pulsador* handle);
static sc_boolean fSM_Pulsador_check_main_region_Wait_tr2_tr2(const FSM_Pulsador* handle);
static sc_boolean fSM_Pulsador_check_main_region_Wait_tr3_tr3(const FSM_Pulsador* handle);
static sc_boolean fSM_Pulsador_check_main_region_Wait_tr4_tr4(const FSM_Pulsador* handle);
static void fSM_Pulsador_effect_main_region_St0_tr0(FSM_Pulsador* handle);
static void fSM_Pulsador_effect_main_region_St3_tr0(FSM_Pulsador* handle);
static void fSM_Pulsador_effect_main_region_Wait_tr0(FSM_Pulsador* handle);
static void fSM_Pulsador_effect_main_region_Wait_tr1(FSM_Pulsador* handle);
static void fSM_Pulsador_effect_main_region_Wait_tr2(FSM_Pulsador* handle);
static void fSM_Pulsador_effect_main_region_Wait_tr3(FSM_Pulsador* handle);
static void fSM_Pulsador_effect_main_region_Wait_tr4(FSM_Pulsador* handle);
static void fSM_Pulsador_enseq_main_region_St0_default(FSM_Pulsador* handle);
static void fSM_Pulsador_enseq_main_region_St3_default(FSM_Pulsador* handle);
static void fSM_Pulsador_enseq_main_region_Wait_default(FSM_Pulsador* handle);
static void fSM_Pulsador_enseq_main_region_default(FSM_Pulsador* handle);
static void fSM_Pulsador_exseq_main_region_St0(FSM_Pulsador* handle);
static void fSM_Pulsador_exseq_main_region_St3(FSM_Pulsador* handle);
static void fSM_Pulsador_exseq_main_region_Wait(FSM_Pulsador* handle);
static void fSM_Pulsador_exseq_main_region(FSM_Pulsador* handle);
static void fSM_Pulsador_react_main_region_St0(FSM_Pulsador* handle);
static void fSM_Pulsador_react_main_region_St3(FSM_Pulsador* handle);
static void fSM_Pulsador_react_main_region_Wait(FSM_Pulsador* handle);
static void fSM_Pulsador_react_main_region__entry_Default(FSM_Pulsador* handle);
static void fSM_Pulsador_clearInEvents(FSM_Pulsador* handle);
static void fSM_Pulsador_clearOutEvents(FSM_Pulsador* handle);	

const sc_integer FSM_PULSADOR_FSM_PULSADORINTERNAL_CDEBOUN = 10;

void fSM_Pulsador_init(FSM_Pulsador* handle)
{
	sc_integer i;

	for (i = 0; i < FSM_PULSADOR_MAX_ORTHOGONAL_STATES; ++i) {
		handle->stateConfVector[i] = FSM_Pulsador_last_state;
	}
	
	
	handle->stateConfVectorPosition = 0;

	fSM_Pulsador_clearInEvents(handle);
	fSM_Pulsador_clearOutEvents(handle);

	/* Default init sequence for statechart FSM_Pulsador */
	handle->internal.vAcuMs = 0;
	handle->internal.vFlg = 0;

}

void fSM_Pulsador_enter(FSM_Pulsador* handle)
{
	/* Default enter sequence for statechart FSM_Pulsador */
	fSM_Pulsador_enseq_main_region_default(handle);
}

void fSM_Pulsador_exit(FSM_Pulsador* handle)
{
	/* Default exit sequence for statechart FSM_Pulsador */
	fSM_Pulsador_exseq_main_region(handle);
}

sc_boolean fSM_Pulsador_isActive(const FSM_Pulsador* handle) {
	sc_boolean result;
	if (handle->stateConfVector[0] != FSM_Pulsador_last_state)
	{
		result =  bool_true;
	}
	else
	{
		result = bool_false;
	}
	return result;
}

/* 
 * Always returns 'false' since this state machine can never become final.
 */
sc_boolean fSM_Pulsador_isFinal(const FSM_Pulsador* handle){
   return bool_false;
}

static void fSM_Pulsador_clearInEvents(FSM_Pulsador* handle) {
	handle->iface.ePush_raised = bool_false;
	handle->iface.ePull_raised = bool_false;
	handle->iface.eSysTick_raised = bool_false;
}

static void fSM_Pulsador_clearOutEvents(FSM_Pulsador* handle) {
}

void fSM_Pulsador_runCycle(FSM_Pulsador* handle) {
	
	fSM_Pulsador_clearOutEvents(handle);
	
	for (handle->stateConfVectorPosition = 0;
		handle->stateConfVectorPosition < FSM_PULSADOR_MAX_ORTHOGONAL_STATES;
		handle->stateConfVectorPosition++) {
			
		switch (handle->stateConfVector[handle->stateConfVectorPosition]) {
		case FSM_Pulsador_main_region_St0 : {
			fSM_Pulsador_react_main_region_St0(handle);
			break;
		}
		case FSM_Pulsador_main_region_St3 : {
			fSM_Pulsador_react_main_region_St3(handle);
			break;
		}
		case FSM_Pulsador_main_region_Wait : {
			fSM_Pulsador_react_main_region_Wait(handle);
			break;
		}
		default:
			break;
		}
	}
	
	fSM_Pulsador_clearInEvents(handle);
}


sc_boolean fSM_Pulsador_isStateActive(const FSM_Pulsador* handle, FSM_PulsadorStates state) {
	sc_boolean result = bool_false;
	switch (state) {
		case FSM_Pulsador_main_region_St0 : 
			result = (sc_boolean) (handle->stateConfVector[0] == FSM_Pulsador_main_region_St0
			);
			break;
		case FSM_Pulsador_main_region_St3 : 
			result = (sc_boolean) (handle->stateConfVector[0] == FSM_Pulsador_main_region_St3
			);
			break;
		case FSM_Pulsador_main_region_Wait : 
			result = (sc_boolean) (handle->stateConfVector[0] == FSM_Pulsador_main_region_Wait
			);
			break;
		default: 
			result = bool_false;
			break;
	}
	return result;
}

void fSM_PulsadorIface_raise_ePush(FSM_Pulsador* handle) {
	handle->iface.ePush_raised = bool_true;
}
void fSM_PulsadorIface_raise_ePull(FSM_Pulsador* handle) {
	handle->iface.ePull_raised = bool_true;
}
void fSM_PulsadorIface_raise_eSysTick(FSM_Pulsador* handle) {
	handle->iface.eSysTick_raised = bool_true;
}



/* implementations of all internal functions */

static sc_boolean fSM_Pulsador_check_main_region_St0_tr0_tr0(const FSM_Pulsador* handle) {
	return (handle->iface.ePush_raised) && (handle->internal.vFlg == 0);
}

static sc_boolean fSM_Pulsador_check_main_region_St3_tr0_tr0(const FSM_Pulsador* handle) {
	return (handle->iface.ePull_raised) && (handle->internal.vFlg == 2);
}

static sc_boolean fSM_Pulsador_check_main_region_Wait_tr0_tr0(const FSM_Pulsador* handle) {
	return (handle->iface.ePull_raised) && ((handle->internal.vAcuMs == FSM_PULSADOR_FSM_PULSADORINTERNAL_CDEBOUN) && (handle->internal.vFlg == 3));
}

static sc_boolean fSM_Pulsador_check_main_region_Wait_tr1_tr1(const FSM_Pulsador* handle) {
	return (handle->iface.ePull_raised) && ((handle->internal.vAcuMs == FSM_PULSADOR_FSM_PULSADORINTERNAL_CDEBOUN) && (handle->internal.vFlg == 1));
}

static sc_boolean fSM_Pulsador_check_main_region_Wait_tr2_tr2(const FSM_Pulsador* handle) {
	return (handle->iface.eSysTick_raised) && (handle->internal.vAcuMs < FSM_PULSADOR_FSM_PULSADORINTERNAL_CDEBOUN);
}

static sc_boolean fSM_Pulsador_check_main_region_Wait_tr3_tr3(const FSM_Pulsador* handle) {
	return (handle->iface.ePush_raised) && ((handle->internal.vAcuMs == FSM_PULSADOR_FSM_PULSADORINTERNAL_CDEBOUN) && (handle->internal.vFlg == 1));
}

static sc_boolean fSM_Pulsador_check_main_region_Wait_tr4_tr4(const FSM_Pulsador* handle) {
	return (handle->iface.ePush_raised) && ((handle->internal.vAcuMs == FSM_PULSADOR_FSM_PULSADORINTERNAL_CDEBOUN) && (handle->internal.vFlg == 3));
}

static void fSM_Pulsador_effect_main_region_St0_tr0(FSM_Pulsador* handle) {
	fSM_Pulsador_exseq_main_region_St0(handle);
	handle->internal.vAcuMs = 0;
	handle->internal.vFlg = 1;
	fSM_Pulsador_enseq_main_region_Wait_default(handle);
}

static void fSM_Pulsador_effect_main_region_St3_tr0(FSM_Pulsador* handle) {
	fSM_Pulsador_exseq_main_region_St3(handle);
	handle->internal.vAcuMs = 0;
	handle->internal.vFlg = 3;
	fSM_Pulsador_enseq_main_region_Wait_default(handle);
}

static void fSM_Pulsador_effect_main_region_Wait_tr0(FSM_Pulsador* handle) {
	fSM_Pulsador_exseq_main_region_Wait(handle);
	fSM_PulsadorIface_aPulsado(handle);
	handle->internal.vFlg = 0;
	fSM_Pulsador_enseq_main_region_St0_default(handle);
}

static void fSM_Pulsador_effect_main_region_Wait_tr1(FSM_Pulsador* handle) {
	fSM_Pulsador_exseq_main_region_Wait(handle);
	handle->internal.vFlg = 0;
	fSM_Pulsador_enseq_main_region_St0_default(handle);
}

static void fSM_Pulsador_effect_main_region_Wait_tr2(FSM_Pulsador* handle) {
	fSM_Pulsador_exseq_main_region_Wait(handle);
	handle->internal.vAcuMs += 1;
	fSM_Pulsador_enseq_main_region_Wait_default(handle);
}

static void fSM_Pulsador_effect_main_region_Wait_tr3(FSM_Pulsador* handle) {
	fSM_Pulsador_exseq_main_region_Wait(handle);
	handle->internal.vFlg = 2;
	fSM_Pulsador_enseq_main_region_St3_default(handle);
}

static void fSM_Pulsador_effect_main_region_Wait_tr4(FSM_Pulsador* handle) {
	fSM_Pulsador_exseq_main_region_Wait(handle);
	handle->internal.vFlg = 0;
	fSM_Pulsador_enseq_main_region_St0_default(handle);
}

/* 'default' enter sequence for state St0 */
static void fSM_Pulsador_enseq_main_region_St0_default(FSM_Pulsador* handle) {
	/* 'default' enter sequence for state St0 */
	handle->stateConfVector[0] = FSM_Pulsador_main_region_St0;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state St3 */
static void fSM_Pulsador_enseq_main_region_St3_default(FSM_Pulsador* handle) {
	/* 'default' enter sequence for state St3 */
	handle->stateConfVector[0] = FSM_Pulsador_main_region_St3;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state Wait */
static void fSM_Pulsador_enseq_main_region_Wait_default(FSM_Pulsador* handle) {
	/* 'default' enter sequence for state Wait */
	handle->stateConfVector[0] = FSM_Pulsador_main_region_Wait;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for region main region */
static void fSM_Pulsador_enseq_main_region_default(FSM_Pulsador* handle) {
	/* 'default' enter sequence for region main region */
	fSM_Pulsador_react_main_region__entry_Default(handle);
}

/* Default exit sequence for state St0 */
static void fSM_Pulsador_exseq_main_region_St0(FSM_Pulsador* handle) {
	/* Default exit sequence for state St0 */
	handle->stateConfVector[0] = FSM_Pulsador_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for state St3 */
static void fSM_Pulsador_exseq_main_region_St3(FSM_Pulsador* handle) {
	/* Default exit sequence for state St3 */
	handle->stateConfVector[0] = FSM_Pulsador_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for state Wait */
static void fSM_Pulsador_exseq_main_region_Wait(FSM_Pulsador* handle) {
	/* Default exit sequence for state Wait */
	handle->stateConfVector[0] = FSM_Pulsador_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for region main region */
static void fSM_Pulsador_exseq_main_region(FSM_Pulsador* handle) {
	/* Default exit sequence for region main region */
	/* Handle exit of all possible states (of FSM_Pulsador.main_region) at position 0... */
	switch(handle->stateConfVector[ 0 ]) {
		case FSM_Pulsador_main_region_St0 : {
			fSM_Pulsador_exseq_main_region_St0(handle);
			break;
		}
		case FSM_Pulsador_main_region_St3 : {
			fSM_Pulsador_exseq_main_region_St3(handle);
			break;
		}
		case FSM_Pulsador_main_region_Wait : {
			fSM_Pulsador_exseq_main_region_Wait(handle);
			break;
		}
		default: break;
	}
}

/* The reactions of state St0. */
static void fSM_Pulsador_react_main_region_St0(FSM_Pulsador* handle) {
	/* The reactions of state St0. */
	if (fSM_Pulsador_check_main_region_St0_tr0_tr0(handle)) { 
		fSM_Pulsador_effect_main_region_St0_tr0(handle);
	} 
}

/* The reactions of state St3. */
static void fSM_Pulsador_react_main_region_St3(FSM_Pulsador* handle) {
	/* The reactions of state St3. */
	if (fSM_Pulsador_check_main_region_St3_tr0_tr0(handle)) { 
		fSM_Pulsador_effect_main_region_St3_tr0(handle);
	} 
}

/* The reactions of state Wait. */
static void fSM_Pulsador_react_main_region_Wait(FSM_Pulsador* handle) {
	/* The reactions of state Wait. */
	if (fSM_Pulsador_check_main_region_Wait_tr0_tr0(handle)) { 
		fSM_Pulsador_effect_main_region_Wait_tr0(handle);
	}  else {
		if (fSM_Pulsador_check_main_region_Wait_tr1_tr1(handle)) { 
			fSM_Pulsador_effect_main_region_Wait_tr1(handle);
		}  else {
			if (fSM_Pulsador_check_main_region_Wait_tr2_tr2(handle)) { 
				fSM_Pulsador_effect_main_region_Wait_tr2(handle);
			}  else {
				if (fSM_Pulsador_check_main_region_Wait_tr3_tr3(handle)) { 
					fSM_Pulsador_effect_main_region_Wait_tr3(handle);
				}  else {
					if (fSM_Pulsador_check_main_region_Wait_tr4_tr4(handle)) { 
						fSM_Pulsador_effect_main_region_Wait_tr4(handle);
					} 
				}
			}
		}
	}
}

/* Default react sequence for initial entry  */
static void fSM_Pulsador_react_main_region__entry_Default(FSM_Pulsador* handle) {
	/* Default react sequence for initial entry  */
	fSM_Pulsador_enseq_main_region_St0_default(handle);
}


