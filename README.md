# WorksPaceModifier
    Modificacion de Libreria, adaptacion solo para la Familia de Dispositivo LPC17xx

## Supported targets:
- LPC1769

## Supported boards:
- LPCXpresso with LPC1769


## Supported toolchains:
- gcc-arm-none-eabi



## Modules:
- BlinkyLed
- DriverEEPROM24LC64
- DriverLed
- SysTickTimer
- FSM_Pulsador

## Brief Modues:
- BlinkyLed: Parpadeo de un Led con la finalidad de detectar si el Sistema queda detenido en algun punto de Codigo.
	
- DriverEEPROM24LC64: Contiene las APIs para escritura/lectura de Datos/Bloques de datos Sobre una Memoria EEPROM conectada a un Bus I2C, cualquiera de los tres que posee esta familia de Micro Controladores [I2C0/1/2]

- DriverLed: son las APIs para controlar led (parpadeo), los cuales se utilizan como testigos de diferentes puntos de codigo. En este caso para interatuar y testear las APIs de MEmoria EEPROM.

- SysTick: Modulo que contiene las Apis para la configuracion, como asi tambien de ISR del SysTick Timer.
 Tambien Encontramos las Apis para leer el valor del Acumulador y la de Consulta de tiempo trasncurrido

- FSM_Pulsador: son las Apis Generadas por la codificacion de la maquina de estado, para manipulacion de Pulsadores.

## Usage


## Acknowledgements

## Autores:
- Luccioni Jesus Emanuel
