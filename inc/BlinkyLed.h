#ifndef __BlinkyLed_h__
#define __BlinkyLed_h__

/** \addtogroup BlinkyLed_h BlinkyLed module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    BlinkyLed.h
 * @brief	Archivo que contine las Definiciones de APIs del modulo
 * BlinkyLed, Inicializacion, Configuracion, y llamado recurrente
 * @version v01.01
 * @date   12/3/2016
 * @note none
 * @par  jel
 *************************************************************************************/

/*==================[global inclusions]======================================*/

/*===================[Open cplusplus]========================================*/
#ifdef __cplusplus
extern "C" {
#endif


/*===================[global macros]=========================================*/

#define PinLedBlk	22 /**<@brief Definimos el Pin Correspondiente al Led de Blinky */
#define PortLedBlk	0 /**<@brief Definimos el Pueto al que coresponde el Pin del
 	 	 	 	 	 	 	 	 Led Blinky */
#define DELTA_TIME	100 /**<@brief Saltos, en micro segundos, de incremento entre
							actualizaciones Automaticas (Cada 5 Segundos) */
#define PERIOD_MAX	2000 /**<@brief Seleccionamos el Valor Maximo para el Periodo */
#define PERIOD_MIN	100	 /**<@brief Seleccionamos el Valor Minimo para el Periodo */
#define DUTY_CICLE_ON
/**<@brief Habilitamos/Deshabilitamos el Ciclo de Trabajo para el
Blink del Led. Lo que Implica que Ton <> Toff */



#ifdef __BlinkyLed_c__ //
/*===================[internal macros]=======================================*/
#define maskLed		(01UL<<PinLedBlk)

/*==================[internal typedef]=======================================*/

/*==============[internal data declaration]==================================*/
static unsigned int Ton,Toff;
static unsigned int AcuMsBlk, AcuMsUpdateLed;


/*============[external functions declaration]==============================*/
void IniBlinkyLed(void);
void UpdateBlinkyLed(void);
void BlinkyLed(void);




#else //  __BlinkyLed_c__
/*===============[external data declaration]================================*/


/*============[external functions declaration]==============================*/


/*******************************************************************//**
 * @brief	Inicializacion de las Funciones correspondiente al modulo
 * "BlinkyLed":
 * + Funcion BlinkyLed();
 * + Funcion UpdateBlinkyLed();
 * @param	None
 * @return	None
 *********************************************************************/
void IniBlinkyLed(void);

/*******************************************************************//**
 * @brief	Funcion Central, invocada desde el Lazo Principal
 * @param	None
 * @return	None
 *********************************************************************/
void BlinkyLed(void);

/*******************************************************************//**
 * @brief	Funcion que actualiza, automaticamente, el Periodo del
 * Blink (Cada 5 Segundo).
 * Tambien invocada desde el Lazo principal.
 * @param	None
 * @return	None
 *********************************************************************/
void UpdateBlinkyLed(void);

#endif // __BlinkyLed_c__


/*=====================[Close cplusplus]=====================================*/

#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

#endif //#ifndef __BlinkyLed_h__
