#ifndef __SysTickTimer_h__
#define __SysTickTimer_h__

#include "../../../modules/lpc1769_2/chip/inc/lpc_types.h"

/** \addtogroup SysTickTimer_h SysTickTimer module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    SysTickTimer.h
 * @brief	Archivo que contine las Definiciones de APIs del modulo SysTickTimer
	 * Configuracion, lectura del Acumulador, chequeo de tiempo (con actuaizacio
	 * automatica del registro correspondiete).
 * @version v01.01
 * @date   21/4/2016
 * @note none
 * @par  jel
 *************************************************************************************/

/*==================[global inclusions]======================================*/

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[global macros]========================================*/

/*==================[global typedef]=======================================*/

#ifdef __SysTickTimer_c__
/*==================[internal macros]========================================*/
#define ST_CTRL_COUNTFLAG	((uint32_t)(1<<16))

/*==================[internal data declaration]==============================*/
static unsigned int AcuMs;

/*=====================[internal typedef]====================================*/

void SysTick_Handler(void);
void ConfigSysTick(void);
unsigned int GetAcuMsSysTick(void);
Bool ConsultTime(unsigned int Time, unsigned int *ptrRef);
void IniAcuTime(unsigned int *ptrAcuMs);

#else //__SysTickTimer_c__

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/*******************************************************************//**
 * @brief	Prototipo de la funcion ISR para la	interupcion
 * del SysTickTimer
 * @param	None
 * @return 	None
 *********************************************************************/
void SysTick_Handler(void);


/*******************************************************************//**
 * @brief	Configura el modulo SysTick Timer, periodo de interupcion
 * @param 	None
 * @return	None
 *********************************************************************/
void ConfigSysTick(void);

/*******************************************************************//**
 * @brief	Funcion para obtener el valor Actual del Acumulador de
 * mili segundos.
 * @param	None
 * @return 	La Cuenta de mili segundos Actual
 *********************************************************************/
unsigned int GetAcuMsSysTick(void);

/*******************************************************************//**
 * @brief	Funcion para Consultar el tiempo transcurido entre llamados,
 * 			y actualizar la referencia temporal
 * @param	Time	Valor en mili segundos, contra el cual se consulta
 *
 * @param 	*ptrRef		Puntero a la variable que toma como referancia
 * , y sobre la cual actualiza el valor, en caso de cumplirse
 * (AcuMs-(Ref))>Time *
 * @return
 * + TRUE Se cumple que [(AcuMs-Ref)>Time] y Actualiza la variable
 * de referencia .
 * + FALSE No se cumple la condicion y no actualiza la variable
 * de referencia
 *********************************************************************/
Bool ConsultTime(unsigned int Time, unsigned int *ptrRef);

/*******************************************************************//**
 * @brief	Funcion para inicializar el Acumulador de Referencia
 * Temporal, perteneciente a la Funcion Periodica (de donde se la invoca)
 * @param *ptrAcuMs, puntero de la vriable de referencia temporal.
 * @return	None
 *********************************************************************/
void IniAcuTime(unsigned int *ptrAcuMs);

#endif //#define __SysTickTimer_c__
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

#endif //#define __SysTickTimer_h__
