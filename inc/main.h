#ifndef __main_h__
#define __main_h__

/** \addtogroup main_h main module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    main.h
 * @brief	Archivo que contine las Definiciones_inc de APIs del modulo main
 * Técnicas Digitales II - Guía de Trabajos Prácticos Ejercicio TP2B
 * @version v01.01
 * @date   12/3/2016
 * @note none
 * @par  Rigalli, Baffico, Luccioni
 *************************************************************************************/


/*==================[end of file]============================================*/
/*==================[inclusions]=============================================*/

/*================[Open cplusplus]===========================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/



/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/


/*******************************************************************//**
 * @brief	Funcion Principal
 * @param 	None
 * @return	La funcion Principal para esta aplicaciones nunca debe
 * retornar.
 *********************************************************************/
int main(void);

/*******************************************************************//**
 * @brief	Funcion para la inicializacion del Hardware Asociado
 * @param 	None
 * @return	None
 *********************************************************************/
static void initHardware(void);

/*==================[Close cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef __main_h__ */
