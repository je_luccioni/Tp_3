/* Copyright 2016, Luccioni Jesus Emanuel
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __DriverEEPROM24LC64_h__
#define __DriverEEPROM24LC64_h__
/* == Util para para usar en el File *.c ==
#if defined(DefinedLabel)
#if !defined(DefinedLabel)
#else
#endif
 */
#include <chip_lpc175x_6x.h>


/*===========================[global inclusions]===========================*/

/** \addtogroup DriverEEPROM24LC16_h  DriverEEPROM24LC16 module
 ** @{ */

/*==============================[]==============================*/
/********************************[Header File]*************************************//**
 * @file    DriverEEPROM24LC16.h
 * @brief   Archivo que contine las definiciones de APIs del modulo DriverEEPROM24LC16
 * <descripcionBreve>
 * @version v0x.0y
 * @date    16/5/2016
 * @par jel
 * @note    none
 *************************************************************************************/

/*==============================[inclusions ]==============================*/



/*============================[Open, cplusplus]============================*/
#ifdef __cplusplus
extern "C" {
#endif


/*================================[macros ]================================*/
#define I2C_EEPROM24LC64	1 /**@brief defined el number of the module I2C select
 >> I2C0 -> 0
 >> I2C1 -> 1
 >> I2C2 -> 2 */
//
#if (I2C_EEPROM24LC64 == 0)
#define FUNCTION_PIN		PIN_FUNC_0
// -- dEFINIMOS LOS PUERTOS
#define	PortSDA 0	/*definimos el Puerto para la Señal SDC_I2C*/
#define PortSCL	0	/* Definimos el Puerto para la señal SCL_I2C */
// -- dEFINIMOS LOS PINES
#define PinSDA	27
#define PinSCL	28
#endif
//
#if (I2C_EEPROM24LC64 == 1)
#define FUNCTION_PIN		PIN_FUNC_3
// -- dEFINIMOS LOS PUERTOS
#define	PortSDA 0	/*definimos el Puerto para la Señal SDC_I2C*/
#define PortSCL	0	/* Definimos el Puerto para la señal SCL_I2C */
// -- dEFINIMOS LOS PINES
#define PinSDA	19
#define PinSCL	20
#endif
//
#if (I2C_EEPROM24LC64 == 2)
#define FUNCTION_PIN		PIN_FUNC_2
// -- dEFINIMOS LOS PUERTOS
#define	PortSDA 0	/*definimos el Puerto para la Señal SDC_I2C*/
#define PortSCL	0	/* Definimos el Puerto para la señal SCL_I2C */
// -- dEFINIMOS LOS PINES
#define PinSDA	10
#define PinSCL	11
#endif
/* Opciones
 * 	>> I2C0
 * 	>> I2C1
 * 	>> I2C2
 * */
/* DEFINIMOS A QUE PINES CORRESPONDE CADA SEÑAL*/

/* Note:
 * EEPROM, del Stick tiene las siguente conexion
 * P0.19 -> I2C1.SDA
 * P0.20 -> I2C1.SCL
 * Config del IOCON
 * OPEN_DRAIN
 * PULL_UP
 * FUNC_3
 * */


/*===============[external and internal, typedef definition]===============*/
/** @brief constant enumeration type for Pin Function Select
 * 	@note	remember which Value after Reset is PIN_FUNC_0 */
typedef enum
{
	OPERATION_SUCCESS = 0, 	/**<@brief Primary (default) function, typically GPIO port */
	OPERATION_FAIL = 1,		/**<@brief First alternate function  */
}enumTypeStOperation;




/** @brief constant enumeration type for Select Device Slave
 * 	@note	remember which Value after Reset is PIN_FUNC_0 */
typedef enum
{
	ADR_EEPROM24LC64_1 = 0x50,	/**<@brief Identification for device 1 */
	ADR_EEPROM24LC64_2 = 0x51,	/**<@brief Identification for device 2 */
	ADR_EEPROM24LC64_3 = 0x52,	/**<@brief Identification for device 3 */
	ADR_EEPROM24LC64_4 = 0x53,	/**<@brief Identification for device 4 */
	ADR_EEPROM24LC64_5 = 0x54,	/**<@brief Identification for device 5 */
	ADR_EEPROM24LC64_6 = 0x55,	/**<@brief Identification for device 6 */
	ADR_EEPROM24LC64_7 = 0x56, 	/**<@brief Identification for device 7 */
	ADR_EEPROM24LC64_8 = 0x57, 	/**<@brief Identification for device 8 */
}enumTypeIdDevice;

/** @brief constant enumeration type for Pin Function Select
 * 	@note	remember which Value after Reset is PIN_FUNC_0 */
typedef enum
{
	SZE_INT_8 = 1,	/**<@brief Identification for type variable char/unsigned char */
	SZE_INT_16 = 2,	/**<@brief Identification for type variable short int /unsigned short int */
	SZE_INT_32 = 4,	/**<@brief Identification for type variable int/unsigned int  */
	SZE_INT_64 = 8,	/**<@brief Identification for type variable long int/ unsigned long int  */
}enumTypeSizeVar;

/** @brief constant enumeration type for Pin Function Select
 * 	@note	remember which Value after Reset is PIN_FUNC_0 */
typedef enum
{
	TRANSFER_SUCCESS = 0,	/**<@brief Transfer success Length = Byte Send  */
	TRANSFER_ERROR = 1,	/**<@brief Transfer error Length <> byte send */
}enumTypeTransfer;


#ifdef __DriverEEPROM24LC64_c__ //


/*============================[internal macros]============================*/


/*=====================[internal, typedef definition]======================*/





/*=======================[internal data declaration]=======================*/



/*=====================[internal functions declaration]====================*/


void IniDriverEEPROM24LC64(void);

enumTypeTransfer ReadEEPROM24LC64(enumTypeIdDevice IdDevice, uint16_t MemPosition,\
		void *ptrDatoR, uint16_t Length);

uint16_t WriteEEPROM24LC64(enumTypeIdDevice IdDevice, uint16_t MemPosition,\
		void *ptrDatoW, uint16_t Length);



#else //  __DriverEEPROM24LC64_c__

/*=====================[external, typedef definition]======================*/






/*=======================[external data declaration]=======================*/
/* remember: extern <typeData> <nmbVar>;
 */

/*=====================[external functions declaration]====================*/
/* remember:
 * extern <typeData> <NmbFuncion> (<typeData> <nmbArg1>,\
 * <typeData> <nmbArg2>);
 * */

/*******************************************************************//**
 * @brief	Initialization of the Interfaces I2C, corresponding
 * to Bus of Memeory EEPROM.
 * @param none
 * @return none
 *********************************************************************/
void IniDriverEEPROM24LC64(void);

/*******************************************************************//**
 * @brief	Read a block of Memory from EEPROM, connect to Bus I2C
 * @param IdDevice	: Identification of the address Memory, option
 * 	+ ADR_EEPROM24LC64_1, Address Memory Slave 1 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_2, Address Memory Slave 2 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_3, Address Memory Slave 3 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_4, Address Memory Slave 4 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_5, Address Memory Slave 5 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_6, Address Memory Slave 6 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_7, Address Memory Slave 7 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_8, Address Memory Slave 8 for EEPROM24LC64
 * @param MemPosition	: Memory Position, inside memory
 * @param *ptrDatoW		: Punter to buffer of data receiver
 * @param Length		: length of the block to read[sizeof(nmbBlock)]
 * @return
 * 	+ TRANSFER_SUCCESS 	: Transfer success Length = Byte Send
 * 	+ TRANSFER_ERROR 	: Transfer error Length <> byte send
 *********************************************************************/
enumTypeTransfer ReadEEPROM24LC64(enumTypeIdDevice IdDevice, uint16_t MemPosition,\
		void *ptrDatoR, uint16_t Length);

/*******************************************************************//**
 * @brief	Write a block of data to Memory EEPROM, connect to Bus I2C
 * @param IdDevice	: Identification of the address Memory, option
 * 	+ ADR_EEPROM24LC64_1, Address Memory Slave 1 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_2, Address Memory Slave 2 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_3, Address Memory Slave 3 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_4, Address Memory Slave 4 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_5, Address Memory Slave 5 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_6, Address Memory Slave 6 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_7, Address Memory Slave 7 for EEPROM24LC64
 * 	+ ADR_EEPROM24LC64_8, Address Memory Slave 8 for EEPROM24LC64
 * @param MemPosition	: Memory Position, inside memory
 * @param *ptrDatoW		: Punter to data send/write
 * @param Length		: length of the block to write [sizeof(nmbBlock)]
 * @return
 * 	+ TRANSFER_SUCCESS 	: Transfer success Length = Byte Send
 * 	+ TRANSFER_ERROR 	: Transfer error Length <> byte send
 *********************************************************************/
enumTypeTransfer WriteEEPROM24LC64(enumTypeIdDevice IdDevice, uint16_t MemPosition,\
		void *ptrDatoW, uint8_t Length);

#endif // __DriverEEPROM24LC16_c__


/*============================[Close cplusplus]============================*/

#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==============================[end of file]==============================*/

#endif // __DriverEEPROM24LC64_h__
