/* Copyright 2016, Luccioni Jesus Emanuel
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __DriverWH1602C_2C4D_h__
#define __DriverWH1602C_2C4D_h__
/* == Util para para usar en el File *.c ==
#if defined(DefinedLabel)
#if !defined(DefinedLabel)
#else
#endif
 */


/*===========================[global inclusions]===========================*/

/** \addtogroup DriverWH1602C_2C4D_h  DriverWH1602C_2C4D module
 ** @{ */

/*==============================[]==============================*/
/********************************[Header File]*************************************//**
 * @file    DriverWH1602C_2C4D.h
 * @brief   Archivo que contine las definiciones de APIs del modulo DriverWH1602C_2C4D
 * <descripcionBreve>
 * @version v0x.0y
 * @date    19/5/2016
 * @par jel
 * @note    none
 *************************************************************************************/
/*
 * ;   <>_ PIN OUT : Modulo
 * ;       ~~~ ~~~
;    _______________________________________________________________________________________________
;   |  01 | 02  | 03  | 04  | 05  | 06  | 07  | 08  | 09  | 10  | 11  | 12  | 13  | 14  | 15  | 16  |
;   |_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|
;   | VSS | VDD | VO  | RS  | RW  |  E  | DB0 | DB1 | DB2 | DB3 | DB4 | DB5 | DB6 | DB7 | An  | Ka  |
;   |_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|
;
 *
 * PSB: Seleccion de Interfaces, PSB = 1 -> Interfases Paralela | PSB = 0 -> Interfases Serie
 * RS: Registro para la Seleccion de Registro, RS = 1 -> Registro de Dato | RS = 0 -> Registro de Instruccion
 * RW: Seleccion de escritura/Lectura, RW = 1 -> Lectura | RW = 0 Escritura
 * E: Disparo de Hailitaccion
 * D7-D4: Nible alto del Bus de Dato p/Interfases de 8-bit, o Bus de dato p/Interfases de 4-Bits
 * D3-D0: Nible bajo del Bus de Dato p/Interfases de 8-Bits
 * VO: Tension Referencia para contraste
 * RST: Reset del Modulo
 * VEE: Tension de Control del LCD, NC *
 * An: Anodo del Led de Fondo
 * Ka: Katodo del Led de Fondo
 * */

/*==============================[inclusions ]==============================*/



/*============================[Open, cplusplus]============================*/
#ifdef __cplusplus
extern "C" {
#endif


/*================================[macros ]================================*/
#define LINE1	0x80//0b10000000
#define LINE2	0xC0//0b11000000
#define NULL_ADR 0xFF /* Indicamos que retomamos la Direccion de memoria
contenida en el Acumulador interno del Modulo LCD*/
/*
//
#define LINE1_WIN1	(unsigned char) 0x80//0b10000000
#define LINE2_WIN1	(unsigned char) 0xC0//0b11000000
//
#define LINE1_WIN2	(unsigned char) 0x90//0b10010000
#define LINE2_WIN2	(unsigned char) 0xD0//0b11010000
//
#define LINE1_WIN3	(unsigned char) 0xA0//0b10100000
#define LINE2_WIN3	(unsigned char) 0xE0//0b11100000
 * */
//
#define LINE1_WIN1	0x80//0b10000000
#define LINE2_WIN1	0xC0//0b11000000
//
#define LINE1_WIN2	0x90//0b10010000
#define LINE2_WIN2	0xD0//0b11010000
//
#define LINE1_WIN3	0xA0//0b10100000
#define LINE2_WIN3	0xE0//0b11100000

/*===============[external and internal, typedef definition]===============*/

typedef enum {
	PTR_DDRAM	= 0, /**@brief */
	PTR_CURSOR, /**@brief */
	PTR_WINDOWS,/**@brief */
} enumTypePtr;

#ifdef __DriverWH1602C_2C4D_c__ //

/*============================[internal macros]============================*/
#define _Fosc	12 // Definimos la Frecuencia en Mhz
// -- Puerto para el Bus de Control del modulo LCD
#define	PortBusCtrl 2
#define PinE 	1		// P2.1 => E, Pin Habilitacion del modulo -> Generacion FECHT
#define PinRS	0		// P2.0 => RS, Pin de selecion Instruccion Dato
//
// -- Puerto para el Bus de Dato del modulo LCD
// Deben ser consecutivos y por defecto el ordes es ascendente caso contrario debemos definirlo
#define PortBusDato	2
#define PinB4	5		// P2.05 => B4
#define PinB5	6		// P2.06 => B5
#define PinB6	7		// P2.07 => B6
#define PinB7	8		// P2.08 => B7
/*===============[Lista de Instrucciones de Inicializacion]================*/
#define TIME_INST_0		20000	// Format microSeg
#define INSTRUCCION_A	0b00110000
#define TIME_INST_A		5000	//  Format microSeg
#define INSTRUCCION_B	0b00110000
#define TIME_INST_B		120		// Format micro Seg
#define INSTRUCCION_C	0b00110000
#define TIME_INST_C		10
//
#define INST_CLEAR_DISP		0b00000001
#define INST_CURSOR_HOME	0b00000010
/*====================[Lista de Instrucciones DISPLAY]=====================*/
// -- Funciones de Display, 0000 1 D C B
#define INST_DISPLAY_ON		0b00001100
#define INST_DISPLAY_OFF	0b00001000
#define	INST_CURSOR_ON		0b00001110
#define INST_CURSOR_OFF		INST_DISPLAY_ON
#define INST_BLINK_ON		0b00001101
#define INST_BLINK_OFF		INST_DISPLAY_ON
#define INST_SHIFT_RIGHT	0b00011100 // Desplazamiento Derecha
#define INST_SHIFT_LEFT		0b00011000 // Desplazamiento Izquierda

/*===============[Lista de TIEMPO de DELY por Instruccion ]================*/
#define TIME_INST_CLEAR		1600
#define TIME_INST_RET_HOME	1520
#define TIME_INST_GENERIC	40
#define TIME_WRITE_DATA		40

/*========================[Definicion de mascaras]=========================*/

/*-- Definimos las Mascara para cada pin*/
#define MaskPinE	(01UL<<PinE)
#define MaskPinRS	(01UL<<PinRS)
#define MaskBusCtrl	(MaskPinE|MaskPinRS)

/*-- Definimos las mascaras para el Bus de Datos */
#define MaskPinB4	(01UL<<PinB4)
#define MaskPinB5	(01UL<<PinB5)
#define MaskPinB6	(01UL<<PinB6)
#define MaskPinB7	(01UL<<PinB7)
#define MaskBusDato	(MaskPinB4|MaskPinB5|MaskPinB6|MaskPinB7)
/*=====================[internal, typedef definition]======================*/





/*=======================[internal data declaration]=======================*/



/*=====================[internal functions declaration]====================*/

//static void DelayNs(uint32_t nDelayNs);
static void DelayUs(uint32_t nDelayUs);
void InicioModuloLCD_WH1602C(void);
void PrintLCD_WH1602C(uint8_t Adr,unsigned char *ptrCadena,uint8_t Length);
uint8_t GetPunteroLCD(enumTypePtr IdPtr);

void ClearDisplay_WH1602C(void);
void ConfigBusDataAndControl(void);
/*===============[Apis para Control del Modulo LCD]===============*/
void ClearDisplay_WH1602C(void);
void CursorHome_WH1602C(void);
void CursorOn_WH1602C(void);
void CursorOff_WH1602C(void);
void SetPosCursor_WH1602C(uint8_t posCursor);
void BlinkOn_WH1602C(void);
void BlinkOff_WH1602C(void);
void SetPosBlink_WH1602C(uint8_t posBlink);
void ShiftLeftDisplay_WH1602C(void);
void ShiftLeftDisplay_WH1602C(void);
void ShiftRigntDisplay_WH1602C(void);


#else //  __DriverWH1602C_2C4D_c__

/*=====================[external, typedef definition]======================*/

void PrintLCD_WH1602C(uint8_t Adr,unsigned char *ptrCadena,uint8_t Length);
uint8_t GetPunteroLCD(enumTypePtr IdPtr);
void InicioModuloLCD_WH1602C(void);
void ClearDisplay_WH1602C(void);
void ConfigBusDataAndControl(void);
/*===============[Apis para Control del Modulo LCD]===============*/
void ClearDisplay_WH1602C(void);
void CursorHome_WH1602C(void);
void CursorOn_WH1602C(void);
void CursorOff_WH1602C(void);
void SetPosCursor_WH1602C(uint8_t posCursor);
void BlinkOn_WH1602C(void);
void BlinkOff_WH1602C(void);
void SetPosBlink_WH1602C(uint8_t posBlink);
void ShiftLeftDisplay_WH1602C(void);
void ShiftLeftDisplay_WH1602C(void);
void ShiftRigntDisplay_WH1602C(void);




/*=======================[external data declaration]=======================*/
/* remember: extern <typeData> <nmbVar>;
 */

/*=====================[external functions declaration]====================*/
/* remember:
 * extern <typeData> <NmbFuncion> (<typeData> <nmbArg1>,\
 * <typeData> <nmbArg2>);
 * */


#endif // __DriverWH1602C_2C4D_c__


/*============================[Close cplusplus]============================*/

#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==============================[end of file]==============================*/

#endif // __DriverWH1602C_2C4D_h__
