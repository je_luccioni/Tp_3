#ifndef __DriverLed_h__
#define __DriverLed_h__

/** \addtogroup DriverLed_h  DriverLed module
 ** @{ */

/********************************[Header File]*************************************//**
 * @file    DriverLed.h
 * @brief	Archivo que contine las Definiciones de APIs del modulo DriverLed
 * Configuracion (GPIO), Inicializacion (LEDs), y Cambio de Estados (On/Off) del Led
 * Deseado
 * @version v01.01
 * @date   12/3/2016
 * @note none
 * @par  jel
 *************************************************************************************/

/*==================[global inclusions]======================================*/

/*===================[Open cplusplus]========================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*===================[global macros]=========================================*/

/*==================[global typedef]=======================================*/
/** @brief enumeracion de estados para indicar el Led sobre el
 * cual se realizara un Toogle */
typedef enum
{
	TOGGLE_LED1 = 0, 	/**<@brief Indicador de Toogle sobre el LED1 */
	TOGGLE_LED2,		/**<@brief Indicador de Toogle sobre el LED2 */
	TOGGLE_LED3,		/**<@brief Indicador de Toogle sobre el LED3 */
	TOGGLE_LED4,		/**<@brief Indicador de Toogle sobre el LED4 */
}enumTypeToggleLed;


#ifdef __DriverLed_c__ //

/*===================[internal macros]=======================================*/
#define maskLed1	(01UL<<PinLed1)
#define maskLed2	(01UL<<PinLed2)
#define maskLed3	(01UL<<PinLed3)
#define maskLed4	(01UL<<PinLed4)
/*==================[internal typedef]=======================================*/

/*==============[internal data declaration]==================================*/
//static unsigned int TimeLed1,TimeLed2,TimeLed3,TimeLed4;
//static unsigned int AcuMsLed1,AcuMsLed2,AcuMsLed3,AcuMsLed4;

static unsigned int vctTimeLed[4];
static unsigned int vctAcuMsLed[4];

/*============[internal functions declaration]===============================*/


/*============[external functions declaration]===============================*/

void IniDriverLed(void);
void ToggleLed(unsigned char IdLed);
//
void ParpadeoLed(enumTypeToggleLed IdLed);
void UpdateTimeParpadeo(enumTypeToggleLed IdLed,unsigned int argTime);
unsigned int GetTimeParpadeo(enumTypeToggleLed IdLed);

#else //  __DriverLed_c__
/*===============[external data declaration]================================*/
/*******************************************************************//**
 * @brief	Api pra la inicializacion de los GPIO relacionado a los
 * LEDs
 * @param 	None
 * @return	None
 *********************************************************************/
void IniDriverLed(void);

/*******************************************************************//**
 * @brief	Funcion que realiza cambio de Estado (On/Off) sobre un
 * Led especifico
 * @param 	IdLed, Identificacion del Led sobre el cual se desea
 * realizar el cambio de estado, puede adoptar los siguentes valores
 * + TOOGLE_LED1
 * + TOOGLE_LED2
 * + TOOGLE_LED3
 * + TOOGLE_LED4
 * @return	None
 *********************************************************************/
void ToggleLed(unsigned char IdLed);



void ParpadeoLed(enumTypeToggleLed IdLed);
void UpdateTimeParpadeo(enumTypeToggleLed IdLed,unsigned int argTime);
unsigned int GetTimeParpadeo(enumTypeToggleLed IdLed);
/*============[external functions declaration]==============================*/

#endif // __DriverLed_c__


/*=====================[Close cplusplus]=====================================*/

#ifdef __cplusplus
}
#endif
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif // __DriverLed_h__
